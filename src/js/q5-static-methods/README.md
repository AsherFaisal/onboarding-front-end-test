# Question 5 - Static Methods

Given the Person constructor below:
  * Create a function name() that simply returns the Person's full name
    * eg:
     ```javascript
       const p = new Person('Jane', 'Doe');
       console.log(p.name()); // Jane Doe
    ```
  * The function must be shared by all instances of Person.
  * Bonus: Use template literal strings to output the full name.
