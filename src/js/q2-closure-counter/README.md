# Question 2 - Closure Counter

Write a function that returns an object that maintains an internal count, and satisfies the following interface:

* count: Number (read only)
* increment()
  * Must use object method shortened form
* decrement()
  * Must use object method shortened form

The internal count must be private.
