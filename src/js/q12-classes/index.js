/**
 * # Question 12 - Classes
 *
 * Convert the following constructor function to a class
 */

class Person{
   constructor(first, last) {
      this.first = first;
      this.last = last;
    } 
    
    get first(){
        return this.first;
    }
    
    get last(){
        return this.last
    }
    
}

Person.prototype.getName = function () {
  return this.first + ' ' + this.last;
};


