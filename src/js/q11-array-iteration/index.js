/* eslint-disable no-undef,no-unused-vars */
const assert = require('assert');

/**
 * # Question 11 - Array iteration
 *
 * Using only `Map`, `Filter`, `Reduce`:
 * 1. Create an array `passingStudents` that only contains students with grades
 * above 50. `passingStudents` should contain objects that match this pattern:
 *  `{ name: student.first + ' ' + student.last, grade: student.grade }`
 * 2. Calculate the class average and store it in `average`
 */

const students = [
  { first: 'Jane', last: 'Doe', grade: 15 },
  { first: 'Mike', last: 'Jackson', grade: 80 },
  { first: 'Don', last: 'Corleone', grade: 7 },
  { first: 'James', last: 'Bond', grade: 90 },
];

// START answer code
const passingStudents = students.Filter(student => {
    if(student.grade >50){
    return name: student.first + ' ' + student.last, grade: student.grade;
    }
    }
)
// END answer code

// Do not modify anything below this point

assert.deepEqual(
  passingStudents,
  [{ name: 'Mike Jackson', grade: 80 }, { name: 'James Bond', grade: 90 }],
);

assert.equal(average, 48);
