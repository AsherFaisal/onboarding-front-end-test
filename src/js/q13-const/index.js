/* eslint-disable */

/**
 * # Question 13 - Const
 *
 */

const data = {
  name: "Mc Jagger"
};

data.name = "Mike Tyson";

// 13a - Why does it allow us to change `name` even though `data` is a const ?

// ----------------------------------------------

data = {
  name: "Michelle Obama"
};

// 13b - What will happen here? Why?

/*
  WRITE YOUR ANSWER HERE
  ----------------------
  ## Answer 13a     so that the const can be re-declared

  ## Answer 13b     It will throw an error 


*/
